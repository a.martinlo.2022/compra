#Aitor Martín

habitual = 'patatas', 'leche', 'pan'

def main():
    listafinal = []
    check = False
    especifica = []
    for producto in habitual:
        listafinal.append(producto)
    while check != True:
        elemento = input("Elemento para comprar:")
        especifica.append(elemento)
        if (elemento == ""):
            check = True

        else:
            for elemento in especifica:
                if elemento not in listafinal:
                    listafinal.append(elemento)

    print(f"La lista de la compra es: ")
    for elemento in listafinal:
        print(elemento)
    print("")
    print(f"El número de elementos habituales: {len(habitual)}")
    print(f"El número de elementos especificos: {len(especifica) -1}")
    print(f"El número de elementos en la lista: {len(listafinal)}")


if __name__ == '__main__':
    main()
    
